package com.hcl.Utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseClass {

	public static WebDriver driver;

	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Users\\Hackathon\\Desktop\\geckodriver-v0.19.1\\geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@AfterTest
	public void teardown() {
		driver.quit();
	}

}
